#include <windows.h>
#include <iostream>
#include <ctime>
#include <vector>

typedef struct _LLnode
{
	int values[100];
	struct _LLnode *pNext;
} LLnode;

typedef struct _DLLnode
{
	int values [ 100 ];
	struct _DLLnode *pBack;
	struct _DLLnode *pNext;
} DLLnode;

typedef struct _node
{
	int values [ 100 ];
} node;

std::vector<node> _vector;

static int value;

/*
	Straight outta stackoverflow
*/

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter ( )
{
	LARGE_INTEGER li;

	if ( !QueryPerformanceFrequency ( &li ) )
		std::cout << "QueryPerformanceFrequency failed!\n";

	PCFreq = double ( li.QuadPart ) / 1000.0;

	QueryPerformanceCounter ( &li );
	CounterStart = li.QuadPart;
}

double GetCounter ( )
{
	LARGE_INTEGER li;
	QueryPerformanceCounter ( &li );
	return double ( li.QuadPart - CounterStart ) / PCFreq;
}

FORCEINLINE void PerformRandomAccessesLL ( LLnode* pFirstNode, DWORD count, DWORD size )
{
	for ( int i = 0; i < count; i++ )
	{
		int idx = rand ( ) % size;

		LLnode *pLLCurrentNode = pFirstNode;

		for ( int k = 0; k < idx; k++ )
		{
			pLLCurrentNode = pLLCurrentNode->pNext;
		}

		 int value2 = pLLCurrentNode->values [ rand ( ) % 100 ];

		 // COMPILER OPTIMIZATION IS A BITCH

		 _asm
		 {
			 push eax
			 mov eax, value2
			 mov value, eax
			 pop eax
		 }
	}
}

FORCEINLINE void PerformRandomAccessesCI ( DWORD* pPtrList, DWORD count, DWORD size )
{
	for ( int i = 0; i < count; i++ )
	{
		int idx = rand ( ) % size;

		int value2 = ( ( node * ) ( pPtrList [ i ] ) )->values[ rand () % 100];

		// COMPILER OPTIMIZATION IS A BITCH

		_asm
		{
			push eax
			mov eax, value2
			mov value, eax
			pop eax
		}
	}
}

FORCEINLINE void PerformRandomAccessesVector ( DWORD count, DWORD size )
{
	for ( int i = 0; i < count; i++ )
	{
		int idx = rand ( ) % size;

		int value2 = _vector [ idx ].values [ rand ( ) % 100 ];

		// COMPILER OPTIMIZATION IS A BITCH

		_asm
		{
			push eax
			mov eax, value2
			mov value, eax
			pop eax
		}
	}
}

int main ( )
{
	srand ( time ( NULL ) );

	std::cout << "Press any key to start the benchmarks." << std::endl;
	std::cin.get ( );

	std::cout << "Allocating 10000 Linked List Entires..." << std::endl;

	StartCounter ( );

	LLnode *pLLFirstNode;
	
	for ( int i = 0; i < 10; i++ )
	{
		pLLFirstNode = new LLnode ( );
		LLnode *pLLCurrentNode = pLLFirstNode;

		for ( int i = 0; i < 10000; i++ )
		{
			for ( int k = 0; k < 100; k++ )
			{
				pLLCurrentNode->values [ k ] = rand ( );
			}

			pLLCurrentNode->pNext = new LLnode ( );

			pLLCurrentNode = pLLCurrentNode->pNext;
		}
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Allocating 10000 Double Linked List Entires..." << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		DLLnode *pDLLFirstNode = new DLLnode ( );
		DLLnode *pDLLCurrentNode = pDLLFirstNode;

		for ( int i = 0; i < 10000; i++ )
		{
			for ( int k = 0; k < 100; k++ )
			{
				pDLLCurrentNode->values [ k ] = rand ( );
			}

			pDLLCurrentNode->pNext = new DLLnode ( );
			pDLLCurrentNode->pNext->pBack = pDLLCurrentNode;

			pDLLCurrentNode = pDLLCurrentNode->pNext;
		}
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Allocating 10000 *custom data collection method* entries..." << std::endl;

	StartCounter ( );

	DWORD *pPtrTable;

	for ( int i = 0; i < 10; i++ )
	{
		pPtrTable = ( DWORD * ) malloc ( sizeof ( void * ) );
		DWORD dwTableSize = 1;

		for ( int i = 0; i < 10000; i++ )
		{
			if ( i == dwTableSize )
			{
				dwTableSize *= 2;

				pPtrTable = ( DWORD * ) realloc ( pPtrTable, 4 * dwTableSize );
			}

			node *n = new node ( );

			for ( int k = 0; k < 100; k++ )
			{
				n->values [ k ] = rand ( );
			}

			pPtrTable [ i ] = ( DWORD ) n;
		}
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Allocating 10000 vector entries..." << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10000; i++ )
	{
		node _node = node ( );

		for ( int k = 0; k < 100; k++ )
		{
			_node.values [ k ] = rand ( );
		}

		_vector.push_back ( _node );
	}

	std::cout << "Done, took " << GetCounter ( ) << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 10 entry sized linked list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesLL ( pLLFirstNode, 1000, 10 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 100 entry sized linked list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesLL ( pLLFirstNode, 1000, 100 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 1000 entry sized linked list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesLL ( pLLFirstNode, 1000, 1000 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 10000 entry sized linked list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesLL ( pLLFirstNode, 1000, 10000 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 10 entry sized custom list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesCI ( pPtrTable, 1000, 10 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 100 entry sized custom list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesCI ( pPtrTable, 1000, 100 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 1000 entry sized custom list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesCI ( pPtrTable, 1000, 1000 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 10000 entry sized custom list" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesCI ( pPtrTable, 1000, 10000 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 10 entry sized vector" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesVector ( 1000, 10 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 100 entry sized vector" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesVector ( 1000, 100 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 1000 entry sized vector" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesVector ( 1000, 1000 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cout << "Performing 1000 random accesses on a 10000 entry sized vector" << std::endl;

	StartCounter ( );

	for ( int i = 0; i < 10; i++ )
	{
		PerformRandomAccessesVector ( 1000, 10000 );
	}

	std::cout << "Done, took " << GetCounter ( ) / 10 << "ms" << std::endl;

	std::cin.get ( );
}